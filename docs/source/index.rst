.. scrudful documentation master file, created by
   sphinx-quickstart on Mon Jan  4 09:59:58 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to scrudful's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   rest_framework


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
